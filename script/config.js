// Custom Inserts:

/*
// This would be perfect, if it worked...
// But: https://github.com/klembot/chapbook/issues/103
engine.extend('1.2.1', () => {
    config.template.inserts = [{
        match: /^peyton$/i,
        render: (text, props, invocation) => `
            <div class="speech-bubble">
                <span>
                    ${text}
                </span>
                <div class="speech-bubble-corner"></div>
                <div class="speech-bubble-corner"></div>
                <div class="speech-bubble-corner"></div>
                <div class="speech-bubble-corner"></div>
                <div class="speech-bubble-pointer">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        `
    }, ...config.template.inserts];
});
*/

// Welp its a game jam, lets hack it:
const observer = new MutationObserver((mutations, observer) => {
    mutations.map(mutation => mutation.target.querySelectorAll('quote')).filter(mutation => !!mutation).forEach(els => els.forEach(el => {
        if (!el.innerHTML?.trim()) {
        }
        if (
            el.querySelector('.speech-bubble')
        ) {
            if (
                el.parentElement.classList.contains('skip-animation') && 
                el.querySelector('.speech-bubble').style.animationDelay
            ) {
                el.querySelector('.speech-bubble').style.animationDelay = null
            } else if (
                el.parentElement.style.animationDelay !== el.querySelector('.speech-bubble').style.animationDelay
            ) {
                el.querySelector('.speech-bubble').style.animationDelay = el.parentElement.style.animationDelay
            }
        } else {
            el.innerHTML = `
                <div
                    class="speech-bubble"
                    style="animation-delay: ${el.parentElement.style.animationDelay ?? '0'}"
                >
                    <span>
                        ${el.innerHTML}
                    </span>
                    <div class="speech-bubble-corner"></div>
                    <div class="speech-bubble-corner"></div>
                    <div class="speech-bubble-corner"></div>
                    <div class="speech-bubble-corner"></div>
                    <div class="speech-bubble-pointer">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            `
        }
    }))
});

observer.observe(document.querySelector('#page'), {
    subtree: true,
    attributes: true
});


// Auto-hide variables in backstage:
document.querySelector('.panel button')?.click()


// Tracking:

getUid = function() {
    let uid = engine.state.get('__uid')
    if (!uid) {
        if (crypto.randomUUID) {
            uid = crypto.randomUUID()
        } else if (crypto.getRandomValues) {
            let uidArr = new Uint32Array(5)
            crypto.getRandomValues(uidArr)
            uid = 'old-' + uidArr.join('-')
        } else {
            uid = (+ new Date()).toString()
        }
        engine.state.set('__uid', uid)
    }
    return uid
}

window.engineStateSet = engine.state.set
engine.state.set = function(k, v) {
    window.engineStateSet.apply(this, arguments)

    if (k === 'trail' && window.location.protocol !== 'file:') {
        const trailEncoded = encodeURIComponent(JSON.toString(v))
        const matomoUrl = new URL('https://lucavazzano.matomo.cloud/matomo.php?idsite=2&rec=1')
        matomoUrl.searchParams.append('action_name', v.slice(-1))
        matomoUrl.searchParams.append('uid', getUid())
        matomoUrl.searchParams.append('dimension2', trailEncoded)
        fetch(matomoUrl.href).catch(err => {
            console.error('Error while trying to track:', err)
        })
    }
}
