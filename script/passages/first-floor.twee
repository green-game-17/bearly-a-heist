:: FF:Entry

floor: 'First Floor'
room: 'Entryway'

--

{embed image: './assets/hallway.png'}

[if passage.visits > 1]

Back in the smallest room of the building, the [[window is somehow still open->FF:Window]], pushing a fresh breeze through.

[else]

Quickly you open the heavy door with your paws and rush inside.

After the door closes behind you, you spot long corridors to both sides and feel the breeze of the [[open window->FF:Window]] in front of you.

[cont]

> [[Go to the left corridor->FF:Room1]]
> [[Go to the right corridor->FF:Room101]]
> [[Go back outside->Outside:Side]]


:: FF:Window

[if squawkedAtWindowPenguin]

{embed image: './assets/window_no-penguin.png'}

[else]

{embed image: './assets/window.png'}

[cont]

As you approach the window, you see a well maintained courtyard outside. The building wraps around it in a big circle.

[if squawkedAtWindowPenguin]

The penguin from earlier seems to be avoiding the courtyard now though.

[else]

Slowly waddling over the tiled ground, a penguin seems to be in deep thought - changing between looking down at the tiles, high up in the sky and sighing in between.

> [[Growl at the evil penguin->Fail:FF:Window]]
> [[Squawk at the evil penguin->FF:Window:Squawk]]

[cont]

> [[Carefully back off->FF:Entry]]


:: FF:Window:Squawk

squawkedAtWindowPenguin: true

--

{embed image: './assets/window_no-penguin.png'}

The penguin stops immediately, squawks back into the night and than runs off to a door on the opposite end of the courtyard.

> [[Back off from the window->FF:Entry]]


:: FF:Room1

room: 'Meeting Room 1'

--

{embed image: './assets/meeting1.png'}

A bland meeting room lays behind the first door.

As you look closer, you spot a [[map on the back of the door->FF:Room1:Map]] though.

> [[Go through the left door->FF:Room2]]
> [[Go through the right door->FF:Entry]]


:: FF:Room1:Map

{embed image: './assets/map.png'}

[CSS]
#page article {
    position: relative;
}
.map-labels {
    position: absolute;
    top: 105px;
    width: 100%;

    display: flex;
    flex-layout: row;
    align-items: space-around;

    color: var(--oc-gray-8);
}
.map-labels div {
    text-align: center;
    flex-grow: 1;
}
[cont]

<div class="map-labels">
<div>Basement</div>
<div>Floor 1</div>
<div>Floor 2</div>
</div>

Very useful, hopefully you memorized it well.

> [[Continue exploring->FF:Room1]]


:: FF:Room2

room: 'Meeting Room 2'

--

{embed image: './assets/meeting2.png'}

Yet another meeting room greets you.

This one is filled with [[all sorts of bits and pieces though->FF:Room2:Figures]], scattered across the big board room table and even some of the seats.

> [[Go further through the left door->FF:Reception]]
> [[Go through the right door->FF:Room1]]


:: FF:Room2:Figures

{embed image: './assets/figure.png'}

Truly horrifying.<br>
Who would even want one of those?!

> [[Turn your gaze away again->FF:Room2]]


:: FF:Reception

room: 'Reception'

--

{embed image: './assets/reception.png'}

[if passage.visits > 1]

The reception penguin firmly keeps their view glued to the _Penguin Paradise_ game on their PC.<br>
The chattering outside continues.

[else]

Now you find yourself in a spacious reception area.

Behind the counter, a penguin sits on a squeaky chair and looks at the monitor in front of them.

Or rather they did, before you started walking.

<quote>I won't snitch on you if you won't snitch on me.</quote>

they quickly exclaim and turn back to their game of _Penguin Paradise_ on their PC.

[cont]

> [[Walk past the counter and into the canteen->FF:Canteen]]
> [[Go back to the meeting room area->FF:Room2]]


:: FF:Canteen

room: 'Canteen'

--

{embed image: './assets/canteen.png'}

Right behind the reception lays a big, open space. It is filled with [[groups of tables and chairs->FF:Canteen:Chair]], orderly placed across the room.

Next to the entrance, a [[coffee bar->FF:Canteen:Coffee]] draws the attention of your fine bear nose.

[if passage.visits > 1]

The rumbling noises still continue.

[else]

Further back, you can hear rumbling and clashing.

[cont]

> [[Investigate the loud noises->FF:Kitchen]]
> [[Sneak past the noisy area->FF:Staircase:Canteen]]


:: FF:Canteen:Coffee

{embed image: './assets/canteen.png'}

A well stocked coffee bar.

Sadly everything is cleaned and locked away - so no coffee specialé for you.

> [[Keep looking around in the canteen->FF:Canteen]]


:: FF:Canteen:Chair

{embed image: './assets/keycard1.png'}

Winding between the grouped furniture, everything seems to be in perfect order.

[unless hasKeycard]
On the very last group you see [[something shiny under a chair->FF:Canteen:Keycard]].
[cont]

> [[Look elsewhere in the canteen->FF:Canteen]]


:: FF:Canteen:Keycard

hasKeycard: true

--

{embed image: './assets/keycard2.png'}

Indeed, fallen between the metal legs of one of the chairs, you can see a lanyard which particularly disorganised penguin left behind.

<div class="acquired">
    a Keycard
</div>

> [[Further look around the canteen->FF:Canteen]]


:: FF:Kitchen

room: 'Kitchen'

--

{embed image: './assets/kitchen.png'}

[if passage.visits > 1]

The cook is still rummaging around with the big pot of soup.

<quote>Hmh, yes... Good soup...</quote>

[cont]

[if romancedCook]

Briefly they look at you, smile and than continue cooking even more motivated.

[cont]

[if passage.visits == 1]

As you approach the source of the noises, you enter the canteen's kitchen.

A penguin with a cooking spoon in between their fins waddles around a big pot of soup and keeps throwing more ingredients in it.

From time to time, they taste a spoonful.

<quote>Ah, good soup.</quote>

[cont]

[if romancedCook == false]

> [[Approach the penguin cook->FF:Kitchen:Cook]]

[cont]

> [[Leave the cook do their thing->FF:Canteen]]


:: FF:Kitchen:Cook

{embed image: './assets/kitchen.png'}

<quote>Oh, hello....</quote>

[after 3 seconds]
<quote>Please don't eat me...</quote>

[after 4 seconds]
the cook squeals.

[after 6 seconds]
<quote>You... can try my g... good soup if you want though</quote>

[after 9 seconds]
Slightly hesitant you take a big sip.

Now, what to tell the cook?

[cont]

<div class="fade-in fork" style="animation-delay: 10000ms">

[["It is good soup, indeed"->FF:Kitchen:Cook:Romanced]] <br>
[["This is BAD soup!"->Fail:Cook]]

</div>


:: FF:Kitchen:Cook:Romanced

romancedCook: true

--

{embed image: './assets/kitchen.png'}

Wide eyed and nodding his head happily, the cook looks into your eyes.

<quote>That's the nicest thing anyone has every said about my food!</quote>

[after 3 seconds]
He seems to have taken a liking of you.

[after 6 seconds]
[["Sadly I have to keep exploring" and go back to the seating area->FF:Canteen]]


:: FF:Room101

room: 'Executive Meeting Room 101'

--

{embed image: './assets/placeholder.png'}

The room is furnished with wide, leather spun seats and a very expensive looking, highly polished desk.

[if hasJetpack]

On the far side of it, the wall is covered in ivy and the bottom is lined with small bushes, which look a bit roughed up now.

[else]

On the far side of it, the wall is covered in ivy and the bottom is lined with [[small hedge bushes->FF:Room101:Bushes]].

[cont]

> [[Go to the left->FF:Entry]]
> [[Go to the right->FF:Room102]]


:: FF:Room101:Bushes

hasJetpack: true

--

{embed image: './assets/placeholder.png'}

As you look closer at the bushes, a backpack pushed between them catches your attention.

When you take the backpack, it feels unusually heavy.<br>
Finally, you notice the joystick dangling from it and the metal outlets at its bottom.

<div class="acquired">
    a backpack jetpack
</div>

> [[Retreat back->FF:Room101]]


:: FF:Room102

room: 'Executive Meeting Room 102'

--

{embed image: './assets/placeholder.png'}

On one side of the room stands a wide, leather spun chair.<br>
Closer to you stands a very cheap looking, folding chair. A [[crumbled piece of paper->FF:Room102:Paper]] is laying in front of it.

> [[Go to the room to the left->FF:Room101]]
> [[Go to the room to the right->FF:Staircase:Meeting]]


:: FF:Room102:Paper

{embed image: './assets/placeholder.png'}

Upon unfolding the letter, you can read its details:

[CSS]
.letter {
    text-align: left !important;
    color: var(--oc-indigo-1);
    opacity: 75%;
    font-size: 90%;
}
[cont]

<div class="letter">
Dear Mr. Pingu, <br>
Due to an unexpected downturn in our business which shocks us the most, we see ourselves forced to terminate your employment. <br>
In accordance to our policies you are entitled to 1 (in words: ONE) fish as severance pay. <br>

Your termination is effective immediately on receival of this letter.

Kind Regards, <br>
Penguin Resources

Penguin Inc HQ, Floor 2
</div>

> [[Lay letter back onto the ground->FF:Room102]]


:: FF:Staircase:Template

room: 'Staircase'

--

{embed image: './assets/door.png'}

A stable glass door blocks your path. <br>
Next to it, a keycard reader is fixed to the wall.


[if hasKeycard]

> [[Use keycard and open the door->FF:Staircase:Inside]]

[else]

You don't have a Penguin keycard.<br>
Maybe someone lost theirs while at lunch?

[cont]


:: FF:Staircase:Inside

floor: 'First Floor'
room: 'Staircase'

--

{embed image: './assets/stairs.png'}

A plain, but worryingly well lit, staircase. <br>
There's a [[lift->FF:Staircase:Lift]] embedded between the stairways. <br>
Next to the lift's door, there is a matte-black, steel door with a keypad next to it.

[unless hasPasscode]
Sadly you don't know the passcode for the steel door.
[cont]

> [[Go up the stairs to the second floor->SF:Landing]]

[if hasPasscode]
> [[Enter passcode into keypad and go to the basement->Base:Landing]]
[cont]

> [[Go back to the canteen area->FF:Canteen]]
> [[Go back to the executive meeting area->FF:Room102]]


:: FF:Staircase:Lift

{embed image: './assets/sign2.png'}

<quote>Weight Limit - exceeded</quote>

the lift's control exclaim.

> [[Back off from the lift->FF:Staircase:Inside]]


:: FF:Staircase:Canteen

{embed passage: 'FF:Staircase:Template'}

> [[Walk back to the seating area->FF:Canteen]]


:: FF:Staircase:Meeting

{embed passage: 'FF:Staircase:Template'}

> [[Walk back to the meeting room area->FF:Room102]]
