# Bearly a Heist

Interactive, retro electro, story game.


### How to run
- `build.cmd` to build it ship-able
- `watch.cmd` to build it with the debug backstage enabled and re-build on changes


### References
- [Chapbook format](https://klembot.github.io/chapbook/guide/text-and-links/text-formatting.html)
- [Tweego Options](https://www.motoslave.net/tweego/docs/#usage-options)


### Acknowledgements
- [Chapbook](https://github.com/klembot/chapbook)
- [Tweego](https://github.com/tmedwards/tweego)
- [Open Color](https://github.com/yeun/open-color)
- [Press Start 2P](https://fonts.google.com/specimen/Press+Start+2P)
